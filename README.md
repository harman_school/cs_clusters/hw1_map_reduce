# hw1_map_reduce

## Problem 1 

**problem_1/**

`mapper_1.py` & `reducer_1.py`
- Contain the mapper and reducer for the first algorithm described

`mapper_2.py` & `reducer_2.py`
- Contain the mapper and reducer for the second algorithm described

`word_count.sh`
- Contains the actual call to the mapreduce framework *yarn*

`wrapSlurm.sh`
- Iterate through different combinations of...
- *Algorithm:* 1, 2
- *numReduceTasks:* 20, 50, 90
- *Text Percentage:* 10, 30, 50, 90, 100
- It also calss *slurmSubmit.sh* to create the job

`slurmSubmit.sh`
- Run the actual slurm job

`postRun.sh`
- Aggregate the results from the mapreduce jobs
- Create a list of the top 100 words
- It also calls *pyPost.py*

`pyPost.py`
- Easier to parse the slurm.outs with python
- Creates the summary file for plotting different iterations 

### Writeup

![](images/problem_1.png)

- The facet wrapped portion of the top part is the number of 

**Results:** 

I found that debugging the mapreduce scripts, both mapper and reducer, was extremely difficult from within the `yarn` call. If either of these .py scripts contained any type of syntax/logic error the only output would get would be a long output of java errors that I had no idea what to do with. I finally understood that it was best to first test the mapper and reducer each individually just in a bash script first, this made everything a lot easier. I used the standard approach as outlined in the *Lin & Dyer* text. For the first algorithm (pairs), I would emit **(word, 1)** and the reducer adds new words and counts the instance of each word applied. In the *stripes* approach, we include the counter in the mapper and then emit **(word, count)** for each instance of a word. I utilized the slurm job manager to submit different combinations of the algorithm (stripes/pairs), text percentage, and numReduceTasks. This allowed for me to run many combinations much faster and then simply aggregate the time and performance of each one of these.

It was interesting to see that the *stripes* algorithm does so much better than the *pairs*, while true that this was also shown in the *Lin & Dyer* example. More so, it is very interesting that the *stripes* approach is affected so little by increasing the amount percentage of the NYT corpus. It is pretty amazing that the time for running on 10% and 100% of the algorithm is identical. However, if we look below on the plot of *Output Records vs Percent NYT* we can see that the number of records in the *pairs* algorithm scales linearly with the percent of the corpus being used. Thus it makes sense that the time is so much greater with this approach. The number of reduce tasks seems to help when we are using the larger sample of the corpus, but it does seem to help increasing this value.

Logsitically, I had a difficult time working with the hadoop filesystem, and how to access and read files on this file-system. In addition, slurm required some additional information I had not been providing on exahead. However, the bigbird wiki documentation was actually really helpful.


## Problem 2 

**problem_2/**

`mapper_st.py` & `reducer_st.py`
- Contain the mapper and reducer for abbreviation St.

`mapper_dr.py` & `reducer_dr.py`
- Contain the mapper and reducer for abbreviation Dr.

`wrapSlurm.sh`
- Iterate through different combinations of...
- *Abbreviation:* St., Dr.
- *numReduceTasks:* 20, 50, 90
- *Text Percentage:* 10, 30, 50, 90, 100
- It also calss *slurmSubmit.sh* to create the job

`slurmSubmit.sh`
- Run the actual slurm job

`postRun.sh`
- Aggregate the results from the mapreduce jobs
- Create a list of the top 100 words
- It also calls *pyPost.py*

`pyPost.py`
- Easier to parse the slurm.outs with python
- Creates the summary file for plotting different iterations 

### Writeup

![](images/problem_2.png)

**Results:** 

This was also an interesting problem. My approach was pretty straightforward. The mapper finds any instance of the abbreviation for example *Dr* (with or without period). I then pad each end of the line if it was found, this may add some additional time if the abbreviation does not need to be padded, however, I was unsure of the trade-off between checking if the sentence needed padding vs just immediately padding. The mapper also does the context encoding described in the problem. I have the problem running with both **Dr** and **St**, although it should work with any ambiguous abbreviation, however, the way it is currently written has it hardcoded to use only these two.

```
The new and shiny Dr. Patty Folds died

emits
(ULLL_UUL-, The new and shiny Dr. Patty Folds died)
```

The reducer then creates keys of the given context key generated and then adds the value. It also makes sure that no more than 100 items belong to any unique key and that no duplicates are added. Like the previous example I am using the cluster to run multiple instances of this mapreduce problem with varying combinations of the two different abbreviations, numReduceTasks, and NYT text percentage. The reducer then outputs the context key and the original phrase as listed in the example. There is a script that aggregates the output and then parses the *slurm.out* files from the jobs to store the parameters and get the runtime.

There performance of this mapreduce problem seemed a bit more sporadic over the percentage of the corpus. I am not sure if this might indicate that I have a bug in my code somewhere ore what, but there are certainly instances of the mapreduce job taking longer at lower percentages of the corpus being used. I would imagine that the performance would vary depending on the frequency of the abbreviation being used. Additionally, the number of input and output records seems to scale linearly with the percent of the corpus being used.

I have very little experience working with NLP or text data in any capacity so some of that was a bit difficult at the beggining and there is a chance I misinterpreted the objective, but overall I found this to be a fun problem to work on. 

