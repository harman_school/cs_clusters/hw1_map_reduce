#!/usr/bin/python3

import fileinput 

# Iterate through each line
for ii in fileinput.input():
	# Iterate through each word
	for jj in ii.split():
		# Emit key and 1
		print('{} {}'.format(jj, 1))
