#!/usr/bin/python3

import fileinput 

# Create empty dict
outDict = {}

# Iterate through each line
for ii in fileinput.input():
	# Iterate throug heach word
	for jj in ii.split():
		
		# Add one or create key for new words
		if jj in outDict.keys():
			outDict[jj] = outDict[jj] + 1
		else:
			outDict[jj] = 1

# Emit keys that are word and overall count
for currKey in outDict.keys():
	print('{} {}'.format(currKey, outDict[currKey]))


