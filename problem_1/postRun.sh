#!/bin/bash

# Parent directory
parDir=/cslu/homes/harmang/hw/hw1

# Summary file
mapOut=/user/harmang/wc_alg_1_wrk_20_corp_100

# Summary output
summaryOut=${parDir}/logfiles/top_words_all.txt

# Create summary of words if it doesnt exist
if [ ! -e ${summaryOut} ]; then
	# Create a summary of all word counts
	hadoop fs -cat ${mapOut}/part-* > ${parDir}/logfiles/top_words_all.txt
fi

# Move slurm logs if they exist
if [ -e ${parDir}/slurm-8* ]; then
	# Move slurm output
	mv ${parDir}/slurm-8* ${parDir}/logfiles/slurm_out/
fi

# Run the python script
${parDir}/pyPost.py
