#!/usr/bin/python3

import collections
import os
import re

##################################################
# Return the top 100 words
##################################################

# Create dictionary of top words
topDict = collections.Counter()

parDir = '/cslu/homes/harmang/hw/hw1/'

# Parse overall textfile
with open(parDir + 'logfiles/top_words_all.txt') as f:
	content = [x.strip() for x in f.readlines()]

# Put the output into a collections counter dict
for item in content:
	topDict[item.split(' ')[0]] = item.split(' ')[1]

# Print the most common_words
with open(parDir + '/logfiles/top_100.txt', 'w') as f:
	for wrd, cnt in topDict.most_common(100):
		f.write(wrd + '\t' + cnt + '\n')


##################################################
# Create log of performance for different runs
##################################################

# Function to return summary from slurm.out
def parse_slurm(fileName):

        with open(fileName) as f:
                content = [x.strip() for x in f.readlines()]

        # Summary
        summ = content[-1].split(' ')

        alg = summ[2] # Algorithm number
        corp = summ[3].split('.')[-1] # Corpus percentage
        wrk = summ[4] # Number of tasks
        dur = summ[-1] # Duration (secs)

        # Get input and output record
        input_rec = [x for x in content if re.search('Map input records=', x)][0]
        output_rec = [x for x in content if re.search('Map output records=', x)][0]

        # Get number from this
        input_num = input_rec.split('=')[-1]
        output_num = output_rec.split('=')[-1]

        out_summary = (alg + ',' +
                        corp + ',' +
                        wrk + ',' +
                        dur + ',' +
                        input_num + ',' +
                        output_num + '\n')

        return out_summary

# Write this output
with open(parDir + '/logfiles/summaryOut.txt', 'w') as f:
        f.write('alg,nyt_perc,ntasks,time,input,output\n')
        for ii in os.listdir(parDir + '/logfiles/slurm_out/'):
                print(ii)
                parseOut = parse_slurm(parDir + '/logfiles/slurm_out/' + ii)
                f.write(parseOut)
