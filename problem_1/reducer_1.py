#!/usr/bin/python3

import sys 
import collections

# This will allow us to sord by count
outDict = collections.Counter()

# Iterate through each line
for ii in sys.stdin:

	# Get word and count
	wd, cnt = ii.split(' ')

	# Add one to count or create key
	if wd in outDict.keys():
		outDict[wd] = outDict[wd] + 1
	else:
		outDict[wd] = 1

# Write the top 100 words
for wd, cnt in outDict.most_common(100):
	print('{} {}'.format(wd, cnt))


