#!/usr/bin/python3

import sys 
import collections

# This will allow us to sord by count
outDict = collections.Counter()

# Iterate through each line 
for ii in sys.stdin:

	# Get word and count
	wd, cnt = ii.split(' ')

	# Add one or create key if it doesnt exist
	if wd in outDict.keys():
		outDict[wd] = outDict[wd] + 1
	else:
		outDict[wd] = 1

# Store the most common one hundred words
for wd, cnt in outDict.most_common(100):
	print('{} {}'.format(wd, cnt))

