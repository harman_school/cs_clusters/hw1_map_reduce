#!/bin/bash

### --------  SLURM  ----------- ###
#SBATCH --job-name=mapJobs
#SBATCH --time=01:00:00
#SBATCH -p map-reduce
#SBATCH --nodes=2
#SBATCH --ntasks=2
### -------------------------- ###

alg=$1 # Which algorithm used
outDir=$2 # Output directory
corpus=$3 # Which nyt_split percentage
num_worker=$4 # How many tasks

# Run the script
/cslu/homes/harmang/hw/hw1/word_count.sh ${alg} ${outDir} ${corpus} ${num_worker}

