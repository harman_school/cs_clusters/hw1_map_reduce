#!/bin/bash

# Store init time
tic=`date +%s`

set -x

JARFILE="/usr/hdp/current/hadoop-mapreduce-client/hadoop-streaming.jar"
logfile=/cslu/homes/harmang/hw/hw1/logfiles/log.txt

# Take input arguements for given job
alg=$1
outDir=$2
corpus=$3
num_worker=$4

# Remove output if it exists
hadoop fs -rm -r /user/harmang/${outDir}

# Run mapreduce
yarn jar $JARFILE \
	-files "./mapper_${alg}.py,./reducer_${alg}.py" \
	-numReduceTasks ${num_worker} \
	-mapper "mapper_${alg}.py" \
	-reducer "reducer_${alg}.py" \
	-input /data/nyt_splits/nyt_eng.${corpus}.txt \
	-output ${outDir}

# Get output time
toc=$((`date +%s` - ${tic}))

# Create log if it doesnt exist
if [ ! -e ${logfile} ]; then
	touch ${logfile}
fi

# Store details from the log in the output
echo ${alg} nyt_eng.${corpus} ${num_worker} ${toc} >> ${logfile}

