#!/bin/bash

# Command to run sbatch job
subCMD=/cslu/homes/harmang/hw/hw1/slurmSubmit.sh

# Iterate through all algorithm options
for alg_num in 1 2; do
	# Iterate through ntasks
	for num_worker in 20 50 90; do
		# Which percentage of split
		for corpus in 10 30 50 70 100; do
			
			# Submit the job
			sbatch ${subCMD} ${alg_num} wc_alg_${alg_num}_wrk_${num_worker}_corp_${corpus} ${corpus} ${num_worker}
		
		done
	done
done	
