#/bin/bash

# Store init time
tic=`date +%s`

set -x

JARFILE="/usr/hdp/current/hadoop-mapreduce-client/hadoop-streaming.jar"

# Take input arguements for given job
abbrv=$1
outDir=$2
corpusIn=$3
corpus=/data/nyt_splits/nyt_eng.${corpusIn}.txt
num_worker=$4

# Remove output if it exists
hadoop fs -rm -r /user/harmang/${outDir}

# Run mapreduce
yarn jar $JARFILE \
	-files "./mapper_${abbrv}.py,./reducer_${abbrv}.py" \
	-numReduceTasks ${num_worker} \
	-mapper "mapper_${abbrv}.py" \
	-reducer "reducer_${abbrv}.py" \
	-input ${corpus} \
	-output ${outDir}

# Get output time
toc=$((`date +%s` - ${tic}))

# Store details from the log in the output
echo ${abbrv} nyt_eng.${corpus} ${num_worker} ${toc}

# Run script to generate aggregate output
/cslu/homes/harmang/hw/hw1_map_reduce/problem_2/postRun.sh ${abbrv} ${outDir} ${corpusIn} ${num_worker}
