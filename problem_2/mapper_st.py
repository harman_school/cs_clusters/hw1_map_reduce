#!/usr/bin/python3

import sys
import fileinput 
import string

# Abbreviation to use
abbrv = 'st'

# Function to convert context vector to shorthand
def convertVect(v):
	
	v = ['!' if x[0] != '-' and x[0] in string.punctuation else x for x in v]
	v = ['l' if x[0].islower() else x for x in v]
	v = ['n' if x[0].isdigit() else x for x in v]
	v = ['u' if x[0].isupper() else x for x in v]
	v = ['p' if x == '!' else x for x in v]

	return [x.upper() for x in v]
	


# Iterate through each line
for ii in fileinput.input():
	
	words = ii.split()

	# Check if phrase is in there
	abrv_ind = [ind for ind, x in enumerate(words) if abbrv == x.lower() or abbrv + '.' == x.lower()]

	if len(abrv_ind) > 0:

		for curr_ind in abrv_ind:
	
			# pad words
			padding = ['-'] * 4
			words_pd = padding + words + padding

			output_beg = words_pd[curr_ind:(curr_ind + 9)]
			convertOut = convertVect(output_beg)
		
			convertOut[4] = '_'

			#print(output_beg)	
			#str_out = '-'.join(output_beg)
			
			print('{}\t{}'.format(''.join(convertOut), ' '.join(output_beg)))
