#!/bin/bash

# Which abbrv
abbrv=$1
outDir=$2
corpus=$3
num_worker=$4

# Parent directory
parDir=/cslu/homes/harmang/hw/hw1_map_reduce/problem_2

# Summary file

# Summary output (only need one for each abbrv)
if [[ corpus -eq 100 && num_worker -eq 50 ]]; then
	
	# Output file
	summaryOut=${parDir}/logfiles/allOut_${abbrv}.txt

	# Create out summary
	hadoop fs -cat /user/harmang/${outDir}/part-* > ${summaryOut}
fi

# Move slurm logs if they exist
#if [ -e "${parDir}"/slurm-* ]; then
#	# Move slurm output
#	mv ${parDir}/slurm-8* ${parDir}/logfiles/slurm_out/
#fi

# Run the python script
#${parDir}/pyPost.py
