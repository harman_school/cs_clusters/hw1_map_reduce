#!/usr/bin/python3

import os
import re


parDir = '/cslu/homes/harmang/hw/hw1_map_reduce/problem_2/'

##################################################
# Create log of performance for different runs
##################################################

# Function to return summary from slurm.out
def parse_slurm(fileName):

        with open(fileName) as f:
                content = [x.strip() for x in f.readlines()]

        # Summary
        summ = content[-2].split(' ')

        abbrv = summ[0] # Algorithm number
        corp = summ[1].split('.')[-2] # Corpus percentage
        wrk = summ[-2] # Number of tasks
        dur = summ[-1] # Duration (secs)

        # Get input and output record
        input_rec = [x for x in content if re.search('Map input records=', x)][0]
        output_rec = [x for x in content if re.search('Map output records=', x)][0]

        # Get number from this
        input_num = input_rec.split('=')[-1]
        output_num = output_rec.split('=')[-1]

        out_summary = (abbrv + ',' +
                        corp + ',' +
                        wrk + ',' +
                        dur + ',' +
                        input_num + ',' +
                        output_num + '\n')

        return out_summary

# Write this output
with open(parDir + '/logfiles/summaryOut.txt', 'w') as f:
        f.write('alg,nyt_perc,ntasks,time,input,output\n')
        for ii in os.listdir(parDir + '/logfiles/slurm_out/'):
                print(ii)
                parseOut = parse_slurm(parDir + '/logfiles/slurm_out/' + ii)
                f.write(parseOut)
