#!/usr/bin/python3

import sys 

# This will allow us to sord by count
outDict = {}

# Iterate through each line
for ii in sys.stdin:

	# Get word and count
	context = ii.split('\t')[0]
	phrase = ii.split('\t')[1]

	# Add one to count or create key
	if context in outDict.keys():
	
		# Dont add > 100 items and check for duplicates
		if len(outDict[context]) < 100 and phrase not in outDict[context]:
			outDict[context].append(phrase)
	else:
		outDict[context] = [phrase]

# Write the output
for key in outDict.keys():
	for phrase in outDict[key]:
		print('{} {}'.format(key, phrase))


