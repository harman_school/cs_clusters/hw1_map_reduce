#!/bin/bash

### --------  SLURM  ----------- ###
#SBATCH --job-name=mapJobs
#SBATCH --time=01:00:00
#SBATCH -p map-reduce
#SBATCH --nodes=2
#SBATCH --ntasks=2
### -------------------------- ###

abbrv=$1 # Which algorithm used
outDir=$2 # Output directory
corpus=$3 # Which nyt_split percentage
num_worker=$4 # How many tasks

# Run the script
runCMD=/cslu/homes/harmang/hw/hw1_map_reduce/problem_2/context_run.sh

# Run
${runCMD} ${abbrv} ${outDir} ${corpus} ${num_worker}

