#!/bin/bash

# Command to run sbatch job
subCMD=/cslu/homes/harmang/hw/hw1_map_reduce/problem_2/slurmSubmit.sh

# Iterate through all algorithm options

for abbrv in dr st; do 
	for num_worker in 20 50 90; do
		for corpus in 10 30 50 70 100; do
		
			outDir=problem_2_${abbrv}_corp_${corpus}_wkr_${num_worker}

			sbatch ${subCMD} ${abbrv} ${outDir} ${corpus} ${num_worker}
		done
	done
done

